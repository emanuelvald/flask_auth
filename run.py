from app import app, csrf, db
from config import ProductionConfig
#from flaskext.mysql import MySQL

#db = MySQL()
if __name__ == '__main__':
    csrf.init_app(app)
    db.init_app(app)

    app.run()