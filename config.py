import os
from datetime import timedelta

class Config(object):
    SECRET_KEY = 'secretkeyforsessions'
    PERMANENT_SESSION_LIFETIME = timedelta(minutes = 5)
    WTF_CSRF_ENABLED = False # Inhabilita la autenticación de CSRF para pruebas en POSTMAN

class DevelopmentConfig(Config):
    PORT = "8000"
    ENV = "development"
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://bcc65ebe2d4ffa:278c984f@us-cdbr-iron-east-01.cleardb.net/heroku_9b3317d08af187d'#?reconnect=true' #"mysql://root:root@localhost/flask"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False # Imprime las consultas realizadas en consola

class ProductionConfig(Config):
    DEBUG = False
    # MySQL configurations
    MYSQL_DATABASE_USER = 'bcc65ebe2d4ffa'
    MYSQL_DATABASE_PASSWORD = '278c984f'
    MYSQL_DATABASE_DB = 'heroku_9b3317d08af187d'
    MYSQL_DATABASE_HOST = 'us-cdbr-iron-east-01.cleardb.net'