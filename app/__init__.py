from flask import Flask, render_template # Import flask and template operators
from flask_sqlalchemy import SQLAlchemy # Import SQLALchemy
from config import DevelopmentConfig # Import app configurations
from flask_wtf import CSRFProtect
from app.mod_index.controllers import mod_index
from app.mod_main.controllers import mod_main # Import modules / components using its blueprint handler variable
from app.mod_auth.controllers import mod_auth

app = Flask(__name__) # Define the WSGI application object

app.config.from_object(DevelopmentConfig) # Set configurations for app

csrf = CSRFProtect()
db = SQLAlchemy(app) # Define the database object which is imported by modules and controllers

app.register_blueprint(mod_main) # Register blueprints
app.register_blueprint(mod_auth)

with app.app_context(): # Build the database if it is not existing
    db.create_all()