from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import url_for
from flask import flash
from flask import g #global used for sessions
from flask import Blueprint
from flask_wtf import CSRFProtect
from config import DevelopmentConfig
from app.mod_auth.models import db
from app.mod_auth.models import Usuario
from app.mod_auth.models import DatosPersonal
from app.mod_auth.forms import LoginForm
from app.mod_auth.forms import RegisterForm
from app.mod_auth.forms import DatosPersonalesForm

mod_auth = Blueprint('mod_auth', __name__)

@mod_auth.route('/login', methods= ['GET', 'POST', 'DELETE'])
def login():
    login_form = LoginForm(request.form)
    if request.method == 'POST' and login_form.validate():
        nombre_usuario = login_form.nombre_usuario.data
        password = login_form.password.data

        usuario = Usuario.query.filter_by(usr_nombre_usuario = nombre_usuario).first()

        if usuario is not None and usuario.verificar_password(password):
            session['usr_codigo'] = usuario.usr_codigo
            session['nombre_usuario'] = usuario.usr_nombre_usuario
            session['confirmado'] = usuario.usr_confirmado
            return redirect( url_for('mod_main.home') )
        else:
            mensaje_error = 'Nombre de usuario o contraseña incorrecta'
            flash(mensaje_error)
    elif request.method == 'GET':
        if 'nombre_usuario' in session:
            return redirect( url_for('mod_main.home') )
        
    return render_template('auth/login.html', form = login_form)

@mod_auth.route('/register', methods = ['GET', 'POST', 'DELETE'])
def register():
    register_form = RegisterForm(request.form)
    if request.method == 'POST':
        usuario = Usuario(register_form.nombre_usuario.data,
                          register_form.email.data,
                          register_form.password.data)
        
        nuevo_nombre_usuario = Usuario.query.filter_by(usr_nombre_usuario = register_form.nombre_usuario.data).first()
        nuevo_email = Usuario.query.filter_by(usr_nombre_usuario = register_form.email.data).first()
        
        if nuevo_nombre_usuario is None:
            valida_nombre_usuario = True
        else:
            valida_nombre_usuario = False
            mensaje_error = 'Nombre de usuario se encuentra en uso, ingresa otro'
            flash(mensaje_error)
        
        if nuevo_email is None:
            valida_email = True
        else:
            valida_email = False
            mensaje_error = 'Email se encuentra en uso con otro usuario, ingresa otro'
            flash(mensaje_error)
        
        if valida_nombre_usuario == True and valida_email == True:
            db.session.add(usuario)
            db.session.commit()
        
        if bool(Usuario.query.filter_by(usr_nombre_usuario = register_form.nombre_usuario.data).first()) == True and valida_nombre_usuario == True and valida_email == True:
            session['usr_codigo'] = usuario.usr_codigo
            session['nombre_usuario'] = register_form.nombre_usuario.data
            #session['confirmado'] = usuario.usr_confirmado
            
            return redirect( url_for('mod_main.home') )
        else:
            mensaje_error = 'Email se encuentra en uso con otro usuario, ingresa otro'
            flash(mensaje_error)
    """
    elif request.method == 'GET':
        if 'nombre_usuario' in session:
            return redirect( url_for('mod_main.home') )
    """

    return render_template('auth/register.html', form = register_form)

@mod_auth.route('/logout')
def logout():
    session.clear()
    # return render_template('index.html', message = "Desconectado correctamente")
    return redirect( url_for('mod_auth.login') )