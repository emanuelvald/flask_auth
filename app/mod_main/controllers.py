from flask import Flask 
from flask import render_template
from flask import request
from flask import make_response
from flask import session
from flask import redirect
from flask import url_for
from flask import flash
from flask import g #global used for sessions
from flask import Blueprint

mod_main = Blueprint('mod_main', __name__)

@mod_main.route('/', methods= ['GET', 'POST', 'DELETE'])
def home():
    if 'nombre_usuario' not in session:
        return redirect( url_for('mod_auth.login') )
    
    return render_template('main/home.html')