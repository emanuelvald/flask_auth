from flask import Flask 
from flask import render_template
from flask import request
from flask import make_response
from flask import session
from flask import redirect
from flask import url_for
from flask import flash
from flask import g #global used for sessions
from flask import Blueprint

mod_index = Blueprint('mod_index', __name__)

@mod_index.route('/', methods= ['GET', 'POST', 'DELETE'])
def index():
    if 'nombre_usuario' in session:
        return redirect( url_for('main.home'))

    return render_template('index/index.html')