from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash
import datetime # Libreria para trabajar con hora

db = SQLAlchemy() # Instancia de la clase SQLchemy

class Usuario(db.Model):
    __tablename__ = 'usuarios'
    
    usr_codigo = db.Column(db.Integer, primary_key = True)
    usr_nombre_usuario = db.Column(db.String(50), unique = True)
    usr_email = db.Column(db.String(50))
    usr_password = db.Column(db.String(100))
    usr_fecha_alta = db.Column(db.DateTime, default = datetime.datetime.now)
    usr_confirmado = db.Column(db.Boolean, default = False)
    
    datos_personales = db.relationship('DatosPersonal', foreign_keys = 'DatosPersonal.usr_codigo')
    mails_enviados_remitente = db.relationship('MailEnviado', foreign_keys = 'MailEnviado.usr_codigo_remitente')
    mails_enviados_destinatario = db.relationship('MailEnviado', foreign_keys = 'MailEnviado.usr_codigo_destinatario')
    mensaje_interno_remitente = db.relationship('MensajeInterno', foreign_keys = 'MensajeInterno.usr_codigo_remitente')
    mensaje_interno_destinatario = db.relationship('MensajeInterno', foreign_keys = 'MensajeInterno.usr_codigo_destinatario')
    peliculas = db.relationship('Pelicula', foreign_keys = 'Pelicula.usr_codigo')

    def __init__(self, usr_nombre_usuario, usr_email, usr_password):
        self.usr_nombre_usuario = usr_nombre_usuario.lower()
        self.usr_email = usr_email.lower()
        self.usr_password = self.__crear_password(usr_password)

    def __crear_password(self, usr_password):
        return generate_password_hash(usr_password)

    def verificar_password(self, usr_password):
        return check_password_hash(self.usr_password, usr_password)

class DatosPersonal(db.Model):
    __tablename__ = 'datos_personales'

    dap_codigo = db.Column(db.Integer, primary_key = True)
    usr_codigo = db.Column(db.Integer, db.ForeignKey('usuarios.usr_codigo'))
    dap_apellido = db.Column(db.String(50))
    dap_nombre = db.Column(db.String(50))
    dap_fecha_nacimiento = db.Column(db.DateTime)
    dap_pais = db.Column(db.String(100))

    def __init__(self, dap_apellido, dap_nombre):
        self.dap_apellido = dap_apellido.lower()
        self.dap_nombre = dap_nombre.lower()