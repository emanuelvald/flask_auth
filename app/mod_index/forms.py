from wtforms import Form
from wtforms import StringField, TextField
from wtforms.fields.html5 import EmailField
from wtforms import validators
from wtforms import HiddenField
from wtforms import PasswordField
from models import Usuario

def length_honeypot (form, field):
    if len(field.data) > 0:
        raise validators.ValidationError('El campo debe estar vacío')

class LoginForm(Form):
    nombre_usuario =  StringField('Nombre de usuario',
                            [
                                validators.Required(message = 'Debe ingresar un nombre de usuario válido'),
                                validators.length(min = 4, max = 25, message = 'El nombre de usuario debe ser entre 4 y 50 caracteres')
                            ]
                           )
    password =  PasswordField('Password',
                              [
                                  validators.Required(message = 'Debe ingresar una contraseña válida')
                              ]
                             )

class RegisterForm(Form):
    nombre_usuario =  StringField('Nombre de usuario',
                            [
                                validators.Required(message = 'Debe ingresar un nombre de usuario válido'),
                                validators.length(min = 6, max = 25, message = 'El nombre de usuario debe ser entre 6 y 25 caracteres')
                            ]
                           )
    email = EmailField('Email',
                       [
                            validators.email(message = 'Debe ingresar un email correcto')
                       ]
                      )                               
    password =  PasswordField('Contraseña',
                              [
                                  validators.Required(message = 'Debe ingresar una contraseña válida'),
                                  validators.length(min = 8, max = 25, message = 'El nombre de usuario debe ser entre 6 y 25 caracteres')
                              ]
                             )
    
    """
    password_confirm = PasswordField('Confirme contraseña',
                            [
                                validators.equal_to('password', message = 'Las contraseñas deben coincidir')
                            ]
                           )
    """

    def validar_nombre_usuario(form, field):
        nombre_usuario = field.data # obtiene lo que escribió el usuairo en el formulario
        usuario = Usuario.query.filter_by(usr_nombre_usuario = nombre_usuario).first()

        if usuario is not None:
            raise validators.ValidationError('El usuario ya se encuentra registrado')
    
    def validiar_email(form, field):
        email = field.data
        usuario = Usuario.query.filter_by(usr_email = email).first()

        if usuario is not None:
            raise validators.ValidationError('El email se encuentra asociado a un usuario existente')

class DatosPersonalesForm(Form):
    nombre =  StringField('Nombre',
                            [
                                validators.length(min = 3, max = 50, message = 'El nombre de usuario debe ser entre 6 y 25 caracteres')
                            ]
                           )

    apellido =  StringField('Apellido',
                            [
                                validators.length(min = 3, max = 50, message = 'El nombre de usuario debe ser entre 6 y 25 caracteres')
                            ]
                           )

    fecha_nacimiento =  StringField('Fecha de Nacimiento',
                            [
                                validators.Required(message = 'Debe ingresar un nombre de usuario válido'),
                                validators.length(min = 6, max = 50, message = 'El nombre de usuario debe ser entre 6 y 25 caracteres')
                            ]
                           )

    pais =  StringField('Pais',
                            [
                                validators.length(max = 100, message = 'El nombre de usuario debe ser entre 6 y 25 caracteres')
                            ]
                           )